import { config } from "@vue/test-utils"

config.mocks.$t = () => {}

afterEach(() => {
  // reset spyOn mocks state
  jest.restoreAllMocks();
  // reset mock calls count
  jest.clearAllMocks();
});
