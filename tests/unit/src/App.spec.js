import { shallowMount } from '@vue/test-utils'
import App from '@/App'

describe('App', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(App)
  })

  it('has an header', () => {
    const header = wrapper.find('[data-testid="c-App__heading"]')

    expect(header.exists()).toBeTruthy()
  })

  it('has an title', () => {
    const title = wrapper.find('[data-testid="c-App__title"]')

    expect(title.exists()).toBeTruthy()
  })

  it('has a wizard', () => {
    const wizard = wrapper.find('[data-testid="c-App__wizard"]')

    expect(wizard.exists()).toBeTruthy()
  })
})
